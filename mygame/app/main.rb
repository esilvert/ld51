# Internal Dependencies
require 'app/ducky/ducky.rb'

def on_game_start
  $ducky.game_class = Game
end

def require_game_files
  require 'app/scenes/main_menu.rb'
  require 'app/game.rb'
end

def tick(args)
  $ducky ||= Ducky.configure

  $ducky.require_game_files_with(&method(:require_game_files))
  $ducky.start_game_with(&method(:on_game_start))

  $ducky.tick(args)
end
