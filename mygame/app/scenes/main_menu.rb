class MainMenu < Ducky::Scene
  def initialize
    super
    @background_color = Color.black

    register_label(
      Ducky::Label.new(
        text: 'LD51',
        position: Vector2.new('50%', '80%'),
        size: 15
      ),
      static: true
    )
    @play_button = register_button(play_button)
                   .on_clicked { puts 'coucou' } # @next_scene = Defend; @complete = true}
    @tutorial_button = register_button(tutorial_button)
    #   .on_clicked { @next_scene = DefendTutorial; @complete = true}
    @exit_button = register_button(exit_button)

    # @next_scene = DefendTutorial
    @complete = false

    # register_panel(Panel.new(size: Vector2.new(10000,800)), static: true)
  end

  def complete?
    @complete
  end

  def must_exit?
    @exit_button&.clicked?
  end

  private

  def play_button
    Ducky::Button.new(
      position: Vector2.new(50.vw, 50.vh),
      width: 85.vw,
      height: 100,
      text: 'Play !',
      asset: 'square/blue.png'
    )
  end

  def tutorial_button
    Ducky::Button.new(
      position: Vector2.new('30%', '30%'),
      width: 40.vw,
      height: 100,
      text: 'Tutorial',
      asset: 'square/green.png'
    )
  end

  def exit_button
    Ducky::Button.new(
      position: Vector2.new('70%', '30%'),
      width: 40.vw,
      height: 100,
      text: 'Exit',
      asset: 'square/red.png'
    )
  end
end
