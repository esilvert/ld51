# Ducky


require 'app/ducky/support.rb'
require 'app/ducky/vector2.rb'
require 'app/ducky/color.rb'
require 'app/ducky/aabb.rb'
require 'app/ducky/input.rb'
require 'app/ducky/node.rb'
require 'app/ducky/fsm.rb'

require 'app/ducky/interfaces/uses_mouse.rb'

require 'app/ducky/root.rb'
require 'app/ducky/engines/core.rb'
require 'app/ducky/engines/physics.rb'

require 'app/ducky/nodes/node_2d.rb'
require 'app/ducky/nodes/label.rb'
require 'app/ducky/nodes/sprite.rb'
require 'app/ducky/nodes/button.rb'
require 'app/ducky/nodes/physics_2d.rb'
require 'app/ducky/nodes/static_body.rb'
require 'app/ducky/nodes/kinematic_body.rb'
require 'app/ducky/nodes/shape.rb'
require 'app/ducky/nodes/rectangle_shape.rb'

require 'app/ducky/game.rb'
require 'app/ducky/scene.rb'


class Object
  def log(msg)
    p "[#{self.class.name}] #{msg}"
  end
end

module Ducky
  SCREEN_WIDTH  = 1280
  SCREEN_HEIGHT = 720
  DELTA_TIME = 1.0 / 60.0

  def self.configure
    Root.new
  end

  def self.reload
    p '[Ducky] Reloading the whole game'
    $ducky = nil
    clear_all
  end

  def self.clear_all
    Ducky::Scene.clear
    Input.clear
    Engine::Physics.clear
    $gtk.stop_music
  end
end

p '[Ducky] Source files have been loaded'
